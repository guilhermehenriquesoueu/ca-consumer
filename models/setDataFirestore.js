const db = require('../conns/firebaseRtdb.js');
const logger = require('../modules/logger');
const setData = async(reference, data) => {
  try {
    await db.ref(`${reference}`).set(`${data}`)
  } catch (error) {
    logger.error({ err, scope: '[Function]setData => [Model]setData.js' });
  }
  
};
module.exports = setData;
