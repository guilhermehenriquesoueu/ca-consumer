const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../../secure/.env-dev') });
const axios = require('axios');
const asyncHandler = require('../../helpers/asyncHandler');
const splitArrayInTenGrop = require('../../helpers/splitArrayInTenGroup');
const replaceSpaces = require('../../helpers/replaceSpaces');
const calcAppm = require('../../helpers/appm');
const calcCtns = require('../../helpers/ctns');
const calcCg = require('../../helpers/cg');
const calcRe = require('../../helpers/re');
const megaFlat = require('../../helpers/megaFlat');
const logger = require('../../modules/logger');
const step1 = require('./step1');
const step2 = require('./step2');


const getDataLive = (async () => {
  try {
    const stats = await step1()
    return { data: stats.data.results, api: stats.api};
  } catch (error) {
    logger.error({error, place: "[FI]getFixturesData.JS - [FN]getDataLive"});
  }
});

const getInfoFixtures = (async(array) => {
  try {
    const fixturesId = [];
    array.data.map(async (fixture) => {
      fixturesId.push(fixture.id);
    });
    const listIdsSplited = splitArrayInTenGrop(fixturesId);
    const dataFixtures = [];
    await Promise.all(listIdsSplited.map(async (id) => {
      const stats = await step2(id)
      dataFixtures.push(stats.data.results);
    }));
    return dataFixtures
  } catch (error) {
    logger.error({error, place: "[FI]getFixturesData.JS - [FN]getInfoFixtures"});
  }
  
});

const infoFixturesStructured = (arr) => {
  try {
    const allFixturesDataFlat = arr.flat();
    const listWithoutESports = allFixturesDataFlat.filter((fixture) => ((fixture||{}).timer||{}).tm >= 1 && !((fixture||{}).league||{}).name.includes('Esoccer'));
    const dataScrap = listWithoutESports.map((fixture) => {
      const fixturesProcessed = [];
      const {
        id_d = (fixture||{}).id,
        timer_d = (fixture||{}).timer,
        league_d = (fixture||{}).league,
        home_d = (fixture||{}).home,
        away_d = (fixture||{}).away,
        flag_home = (((fixture||{}).home||{}).image_id||''),
        flag_away = (((fixture||{}).away||{}).image_id||''),
        home_d_o = ((fixture||{}).o_home||{}).name,
        away_d_o = ((fixture||{}).o_away||{}).name,
        home_pos = ((fixture||{}).extra||{}).home_pos,
        away_pos = ((fixture||{}).extra||{}).away_pos,
        goals_home__d = (((fixture||{}).stats||{}).goals||[])[0],
        goals_away__d = (((fixture||{}).stats||{}).goals||[])[1],
        home_attacks_d = (((fixture||{}).stats||{}).attacks||[])[0],
        away_attacks_d = (((fixture||{}).stats||{}).attacks||[])[1],
        home_dangerous_attacks_d = (((fixture||{}).stats||{}).dangerous_attacks||[])[0],
        away_dangerous_attacks_d = (((fixture||{}).stats||{}).dangerous_attacks||[])[1],
        home_on_target_d = (((fixture||{}).stats||{}).on_target||[])[0],
        away_on_target_d = (((fixture||{}).stats||{}).on_target||[])[1],
        home_off_target_d = (((fixture||{}).stats||{}).off_target||[])[0],
        away_off_target_d = (((fixture||{}).stats||{}).off_target||[])[1],
        home_redcards_d = (((fixture||{}).stats||{}).redcards||[])[0],
        away_redcards_d = (((fixture||{}).stats||{}).redcards||[])[1],
        home_yellowcards_d = (((fixture||{}).stats||{}).yellowcards||[])[0],
        away_yellowcards_d = (((fixture||{}).stats||{}).yellowcards||[])[1],
        home_corners_d = (((fixture||{}).stats||{}).corners||[])[0],
        away_corners_d = (((fixture||{}).stats||{}).corners||[])[1],
        home_penalties_d = (((fixture||{}).stats||{}).penalties||[])[0],
        away_penalties_d = (((fixture||{}).stats||{}).penalties||[])[1],
        home_possession_rt_d = (((fixture||{}).stats||{}).possession_rt||[])[0],
        away_possession_rt_d = (((fixture||{}).stats||{}).possession_rt||[])[1],
        events_d = (fixture||{}).events,
        bet365_id_d = (fixture||{}).bet365_id,
      } = fixture;
  
      fixturesProcessed.push({
        id: id_d,
        timer: timer_d,
        league: league_d.name,
        home: home_d.name,
        away: away_d.name,
        img_home: `https://assets.b365api.com/images/team/m/${flag_home}.png`,
        img_away: `https://assets.b365api.com/images/team/m/${flag_away}.png`,
        home_o: home_d_o,
        away_o: away_d_o,
        home_position: home_pos,
        away_position: away_pos,
        goals_home: goals_home__d,
        goals_away: goals_away__d,
        home_attacks: Number(home_attacks_d),
        away_attacks: Number(away_attacks_d),
        home_dangerous_attacks: Number(home_dangerous_attacks_d),
        away_dangerous_attacks: Number(away_dangerous_attacks_d),
        home_on_target: home_on_target_d,
        away_on_target: away_on_target_d,
        home_off_target: home_off_target_d,
        away_off_target: away_off_target_d,
        home_redcards: home_redcards_d,
        away_redcards: away_redcards_d,
        home_yellowcards: home_yellowcards_d,
        away_yellowcards: away_yellowcards_d,
        home_corners: home_corners_d,
        away_corners: away_corners_d,
        home_penalties: home_penalties_d,
        away_penalties: away_penalties_d,
        home_possession: Number(home_possession_rt_d),
        away_possession: Number(away_possession_rt_d),
        events: events_d,
        bet365_id: bet365_id_d,
        appm_home: calcAppm(home_dangerous_attacks_d, timer_d.tm),
        appm_away: calcAppm(away_dangerous_attacks_d, timer_d.tm),   
        cns_home: calcCtns(home_d.name,home_d_o,events_d), 
        cns_away: calcCtns(away_d.name,away_d_o,events_d),   
        cg_home: calcCg(home_on_target_d, home_off_target_d, home_d.name, home_d_o, events_d),
        cg_away: calcCg(away_on_target_d, away_off_target_d, away_d.name, away_d_o, events_d),
        re_home: calcRe(home_dangerous_attacks_d, timer_d.tm, home_possession_rt_d),
        re_away: calcRe(away_dangerous_attacks_d, timer_d.tm, away_possession_rt_d),
        link_bet365: `https://www.bet365.com/#/AX/K^${replaceSpaces(home_d.name)}/`,
      });
      return fixturesProcessed;
    });
    const merged = megaFlat(dataScrap);
    function compare(a,b) {
      if (b.timer.tm < a.timer.tm) return -1; 
      if (b.timer.tm > a.timer.tm) return 1;
      return 0;
    }
    return merged.sort(compare);
  } catch (error) {
    logger.error({error, place: "[FI]getFixturesData.JS - [FN]infoFixturesStructured"});
  }
};

const getFixturesLive = asyncHandler(async () => {
  try {
    const fixturesIdArray = await getDataLive();
    const fixturesStats = await getInfoFixtures(fixturesIdArray);
    const finalArray = await infoFixturesStructured(fixturesStats);
    if(finalArray.length === 0) return [];
    return {data: finalArray, apis: [fixturesIdArray.api]} 
  } catch (error) {
    logger.error({error, place: "[FI]getFixturesData.JS - [FN]getFixturesLive"});
  }
});

module.exports = {
  getFixturesLive,
};
