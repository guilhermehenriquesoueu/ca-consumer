const axios = require('axios');
const path = require('path');
const logger = require('../../modules/logger');
require('dotenv').config({ path: path.resolve(__dirname, '../../secure/.env-dev') });


const node1 = async () => {
  return axios.get(`https://api.b365api.com/v1/events/inplay?sport_id=1&token=${process.env.BET365_API_KEY}`)
    .catch(function () {
      return {data:{success:0}};
    });
};

const node2 = async () => {
  return axios.get(`http://api-d.betsapi.com/v1/events/inplay?sport_id=1&token=${process.env.BET365_API_KEY}`)
    .catch(function () {
      return {data:{success:0}};
    });
}

const node3 = async () => {
  return axios.get(`http://api-d.b365api.com/v1/events/inplay?sport_id=1&token=${process.env.BET365_API_KEY}`)
    .catch(function () {
      return {data:{success:0}};
    });
}

const data = async() => {
  try {
    const resNode1 = await node1();
    if(resNode1.data.success == 1) return {data: resNode1.data, api: "betsapiHttps"};
    
    const resNode2 = await node2();
    if(resNode2.data.success == 1) return {data: resNode2.data, api: "api-d.betsapi"};

    const resNode3 = await node3();
    if(resNode3.data.success == 1) return {data: resNode2.data, api: "api-d.b365api"};
  } catch (err) {
    logger.error({err, place: "[FI]step1 - [FN]data"});
  }
  
}

module.exports = data


