const axios = require('axios');
const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../../secure/.env-dev') });


const node1 = axios.get(`https://api.b365api.com/v1/events/inplay?sport_id=1&token=${process.env.BET365_API_KEY}`, {
  validateStatus: function (status) {
    return status == 200;
  }
}).catch(() => {return {status: false}})

const node2 = axios.get(`http://api-d.betsapi.com/v1/events/inplay?sport_id=1&token=${process.env.BET365_API_KEY}`, {
  validateStatus: function (status) {
    return status == 200;
  }
}).catch(() => {return {status: false}})

const node3 = axios.get(`http://api-d.b365api.com/v1/events/inplay?sport_id=1&token=${process.env.BET365_API_KEY}`, {
  validateStatus: function (status) {
    return status == 200;
  }
}).catch(() => {return {status: false}})

const data = async() => {
  const resNode1 = await node1;
  if(resNode1.status == 200) return {data: resNode1.data, api: "betsapiHttps"};
  
  const resNode2 = await node2;
  if(resNode2.status == 200) return {data: resNode2.data, api: "betsapi"};

  const resNode3 = await node3;
  if(resNode3.status == 200) return {data: resNode2.data, api: "betsapi"};

  return ''
}

data()
