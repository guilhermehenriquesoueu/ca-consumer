const axios = require('axios');
const path = require('path');
const logger = require('../../modules/logger');
require('dotenv').config({ path: path.resolve(__dirname, '../../secure/.env-dev') });


const node1 = async (id) => {
  return axios.get(`https://api.b365api.com/v1/event/view?token=${process.env.BET365_API_KEY}&event_id=${id}`)
    .catch(function () {
      return {data:{success:0}};
    });
};

const node2 = async (id) => {
  return axios.get(`http://api-d.betsapi.com/v1/event/view?token=${process.env.BET365_API_KEY}&event_id=${id}`)
    .catch(function () {
      return {data:{success:0}};
    });
};

const node3 = async (id) => {
  return axios.get(`http://api-d.b365api.com/v1/event/view?token=${process.env.BET365_API_KEY}&event_id=${id}`)
    .catch(function () {
      return {data:{success:0}};
    });
};

const data = async(id) => {
  try {
    const resNode1 = await node1(id);
    if(resNode1.data.success == 1) return {data: resNode1.data, api: "betsapiHttps"};
    
    const resNode2 = await node2(id);
    if(resNode2.data.success == 1) return {data: resNode2.data, api: "api-d.betsapi"};

    const resNode3 = await node3(id);
    if(resNode3.data.success == 1) return {data: resNode2.data, api: "api-d.b365api"};
  } catch (err) {
    logger.error({err, place: "[FI]step2 - [FN]data"});
  }
  
}

module.exports = data



