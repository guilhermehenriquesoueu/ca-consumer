const calcCtns = require('./ctns');
const logger = require('../modules/logger');

const calcCg = (onTarget, offTarget, name='', o_name=name, events='') => {
  try {
    const nameTeam = name;
    const eventsCopy = events;
    const onTargetParsed = Number(onTarget);
    const offTargetParsed = Number(offTarget);
    const calcCtnsFinished = calcCtns(nameTeam, o_name, eventsCopy);
    const value = onTargetParsed + offTargetParsed + calcCtnsFinished;
    return value;
  } catch (error) {
    logger.error({err, place: "[FI]cg.js - [FN]calcCg"});
  }
};

module.exports = calcCg;