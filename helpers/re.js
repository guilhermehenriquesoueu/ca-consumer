const calcAppm = require('./appm')
const logger = require('../modules/logger');
const calcRe = ( dangerous_attack, time, pb ) => {
  try {
    const nDangerous_attack = Number(dangerous_attack);
    const nTime = Number(time);
    const nPb = Number(pb);
    const appm = calcAppm(nDangerous_attack, nTime);
    const re = appm * nPb || '';
    if(re === '') return re;
    return (re||{}).toFixed(0);
  } catch (err) {
    logger.error({err, place: "[FI]re.js - [FN]calcRe"});
  }
  
} 

module.exports = calcRe;