const splitArrayInTenGroup = (array=['']) => {
  try {
    const perChunk = 10;
    const result = array.reduce((all,one,i) => {
      const ch = Math.floor(i/perChunk); 
      all[ch] = [].concat((all[ch]||[]),one); 
      return all
    }, [])
    return result;
  } catch (err) {
    logger.error({err, place: "[FI]splitArrayInTenGroup.js - [FN]splitArrayInTenGroup"});
  }
};

module.exports = splitArrayInTenGroup;
