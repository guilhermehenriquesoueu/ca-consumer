const logger = require('../modules/logger');
const replaceSpaces = (string) => {
  try {
    const newString = string.replace(/\s/g, '%20');
    return newString;
  } catch (err) {
    logger.error({err, place: "[FI]replaceSpaces.js - [FN]replaceSpaces"});
  }
  
};

module.exports = replaceSpaces;
