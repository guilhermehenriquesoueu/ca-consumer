const logger = require('../modules/logger');
const appm = (atp, time) => {
  try { 
    const nAtp = Number(atp);
    const nTime = Number(time);
    const calc = nAtp / nTime;
    if(isNaN(calc)) {
      return ''
    } else {
      return Number(calc.toFixed(2));
    }
  } catch (err) {
    logger.error({err, place: "[FI]appm.js - [FN]appm"});
  }
};

module.exports = appm;
