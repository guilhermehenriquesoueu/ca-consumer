const flatArray = (array) => array.flat();

module.exports = flatArray;
