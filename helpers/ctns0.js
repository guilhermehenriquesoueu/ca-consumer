const calcCtns = (name, events) => {
  try {
    if(events.length === 0 || events === null || events === undefined){
      const ctns = 0;
      return ctns;
    } else {
      let ctns = 0;
      const eventsCornersTeam = events.filter(event => event.text !== null && event.text.includes(name)) || '';
      const eventsFilter = eventsCornersTeam.filter(event => event.text !== null && event.text.includes('Corner')) || '';
      if(eventsFilter.length === 0){
        return ctns;
      } else {
        const eventsFilter2 = eventsFilter.filter(event => event.text !== null && !event.text.includes('Corners'));
        const dataTimeCorner = [];
        eventsFilter2.map(event => {
          const time = Number(event.text.substring(0,2).replace("'", ""));
          dataTimeCorner.push(time);
        });
        let pB = 1;
        for( i = 0; i < dataTimeCorner.length; i++ ) {
          if(dataTimeCorner[pB] - dataTimeCorner[i] >= 3){
            ctns++
          }
          pB++;
        }
        return ctns + 1;
        }
    }
  } catch (error) {
    console.log({error, place: 'ctns'})
  }
}

module.exports = calcCtns;