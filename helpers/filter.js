const filter = (data) => {
  const onlyFile = []
  data.map((el) => {
    if(el.appm_home >= 1 || el.appm_away >= 1){
      onlyFile.push(el)
    }
  })

  const noFile = []
  data.map((el) => {
    if(el.appm_home < 1 && el.appm_away < 1){
      noFile.push(el)
    }
  })

const final = [...onlyFile, ...noFile]
return final
}

module.exports = filter