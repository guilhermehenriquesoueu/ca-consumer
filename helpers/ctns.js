const logger = require('../modules/logger');
const calcCtns = (name='', o_name=name, events) => {
  try {
    let ctns = 0;
    let eventsCornersTeam = [];
    // console.log(name, o_name, events)
    if(events.length == 0 || name == '' || name == null || o_name == '' || o_name == null){
      return ctns;
    } 
    eventsCornersTeam = events.filter(event => event.text !== null && event.text.includes(name));
    if(eventsCornersTeam.length == 0){
      eventsCornersTeam = events.filter(event => event.text !== null && event.text.includes(o_name));
      if(eventsCornersTeam.length === 0) {
        return ctns
      } 
    }
    const eventsOnlyCorner = eventsCornersTeam.filter(event => event.text !== null && event.text.includes('Corner'));
    if(eventsOnlyCorner.length == 0){
      return ctns
    }
    const eventsRemoveCorners = eventsOnlyCorner.filter(event => event.text !== null && !event.text.includes('Corners') && !event.text.includes('Card'));
    const cornersDataTime = [];
    eventsRemoveCorners.map(event => {
      const time = Number(event.text.substring(0,2).replace("'", ""));
      cornersDataTime.push(time);
    });
    let nextPosition = 1;
    cornersDataTime.map( element => {
      if(cornersDataTime[nextPosition] - element >= 3){
        ctns++
      }
      nextPosition ++
    });
    return ctns + 1;
  } catch (err) {
    logger.error({err, place: "[FI]ctns.js - [FN]calcCtns"});
  }
}

module.exports = calcCtns;
