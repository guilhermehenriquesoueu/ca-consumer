const admin = require("firebase-admin");
const serviceAccount = require("../secure/firebase-firestore.json");
const logger = require('../modules/logger');

try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://canto-estatistico-eua-331403-default-rtdb.firebaseio.com/",
  });
  const db = admin.database();
  logger.info('Running', { scope: 'RTDB ON => firebaseRtdb.js' });
  module.exports = db;
} catch (err) {
  logger.error({ err, scope: 'RTDB OFF => firebaseRtdb.js' });
}