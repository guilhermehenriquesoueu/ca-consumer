const {getFixturesLive} = require('../src/bet365Data/getFixturesData');
const libRtdb = require('../libs/libRtdb');
const moment = require('moment-timezone');
const logger = require('../modules/logger');

const saveData = async () => {
  try {
    const getData = await getFixturesLive();
    if(getData.data === undefined) return;
    const getDataStringify = JSON.stringify(getData.data);
    await libRtdb.setDataFirestore('/matches/stats', getDataStringify);
    const hoursNow = moment().tz('America/Sao_Paulo').format('DD MM YYYY h:mm:ss a')
    logger.info(`${hoursNow} || [${getData.apis}]`);
  } catch (error) {
    logger.error({error, place: "[FI]server.JS - [FN]saveData"});
  }
}

setInterval(async () => {
  await saveData();  
}, 30000);
