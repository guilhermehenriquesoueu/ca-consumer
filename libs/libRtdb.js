const setData = require('../models/setDataFirestore');

const setDataFirestore = async (reference, key, data) => setData(reference, key, data);

module.exports = {
  setDataFirestore,
}